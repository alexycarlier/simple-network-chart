
# simple-network-chart

## Overview

A simple JS library for embedding a network chart, given axis definitions and target/actual values. It allows you to embed a clear and intuitive network chart anywhere you can embed HTML and JavaScript. 

The display includes :

 - **name** of your data array
 - **clickable** label and bullet points in a function you can create (selectChartIndex, see the example)
 - **mouseover** style

The simple-network-chart in use looks like this (this is the demo file provided in the repo as `/dist/index.html`, which is also useful if you want to modify the code for your own use (NPM and WebPack is the short answer to the first question you'll ask).

![Simple NEtwork Chart](/docs/network.png?raw=true "Simple Network Chart")

## Usage

You can use the the library directly from githack by referencing the following URI:

`https://gl.githack.com/alexycarlier/simple-network-chart/raw/master/dist/radar.js`

To use the netwrok chart, create an empty page element in which to embed the radar, and include the library:

```html
<script  src="https://gl.githack.com/alexycarlier/simple-network-chart/raw/master/dist/network_chart.js"></script>
```

Now, simply define your data in a script element. This element have to be an array of object, you will be able to retreive this data when user click on label on the chart.

```html
<script>
 var MY_DATA = [
  {
    name: "Metric 1",
    range: [
      "Value 0",
      "Value 1",
      "Value 2",
      "Value 3"
    ],
    target: 2,
    actual: 1
  },
  ...
]

(next below)
```

The `radar.show` function takes a selector for the element to use as a container, and an object containing a desired pixel size, and the metrics to display, like so.

```html

network_chart.show('#demo', {
  size: 700,
  metrics: MY_DATA
});

(next below)
```
And then, define the function that will be called when you click on a label, this function have te be named selectChartIndex :

```html
var selectChartIndex = function(i) {
  console.log(MY_VALUES[i])
}

</script>
```
Each metric requires a **name**, a **range** (a simple ordered array of maturity values, from 0 onwards), a **target** maturity level, and an **actual** maturity level. The library is quite adaptive. It should cope reasonably well with metrics with different cardinalities, maturity measures with different numbers of metrics.

## Acknowledgements

simple-network-chart contains a tree-shaken version of various d3 libraries (it is built using d3).

The simple-network-chart is a modified version of the radar chart work done by # Andrew Cherry (https://github.com/kolektiv/maturity-radar) go see his amazing work.