import { cartesian } from '../position';
import { max } from '../../prelude/maths';

// Axes

// Common base d3 elements generated from the array of names for each maturity
// axis, to which the axial lines and labels are applied.

const common = (metrics, surface) => {
    return surface
        .append('g')
        .attr('class', 'axes')
        .selectAll('.surface')
        .data(metrics.map(m => m.name))
        .enter()
        .append('g')
        .attr('class', 'axis');
};

// Axial lines radiating from the network_chart centre point, scaled for the common
// d3 scale in use.

const lines = (common, { arc, range, scale }) => {
    return common
        .append('line')
        .attr('x1', 0)
        .attr('y1', 0)
        .attr('x2', (_, i) => cartesian(arc, i, scale(max(range))).x())
        .attr('y2', (_, i) => cartesian(arc, i, scale(max(range))).y())
        .attr('class', 'line')
        .style('stroke', 'rgb(161 187 189)')
        .style('stroke-width', 1);
};

// Axial labels, giving the name of the maturity axis at the end of each axial
// line.

const labels = (common, { arc, range, scale }) => {
    var label_elem = common
        
    var cur_x = []
    var cur_y = []

    var next_data = []

    label_elem
    .append('text')
        .attr('text-anchor', 'middle')
        .attr('x', (_, i) => {
            cur_x.push(cartesian(arc, i, scale(max(range)) * 1.25).x())
            return cur_x[i];
        })
        .attr('y', (_, i) => {
            cur_y.push(cartesian(arc, i, scale(max(range)) * 1.25).y())
            return cur_y[i];
        })
        .style('fill', 'black')
        .style('font-family', 'sans-serif')
        .style('font-size', '9px')
        .style('font-weight', '600')
        .text( (d, i) => {
            var splitted_text = d.split(" ");
            var display_text = ""
            var next_text = ""
            for (const elem of splitted_text)
            {
                if ( display_text.length + elem.length < 25)
                {
                    display_text = display_text + ' ' + elem
                }
                else
                {
                    next_text = next_text + ' ' + elem;
                }
            }
            next_data.push(next_text)
            
            return display_text;
        })
        .style('cursor', 'pointer')
        .on('click', (d, i) => selectChartIndex(i))
        .on('mouseover', (d, i) => {
            document.getElementById("index"+i).setAttribute('r', 14);
        })
        .on('mouseleave', (d, i) => {
            document.getElementById("index"+i).setAttribute('r', 11);
        })
        .style('cursor', 'pointer')
        .on('click', (d, i) => selectChartIndex(i))
        .on('mouseover', (d, i) => {
            document.getElementById("index"+i).setAttribute('r', 14);
        })
        .on('mouseleave', (d, i) => {
            document.getElementById("index"+i).setAttribute('r', 11);
        })

    label_elem
    .append('text')
        .attr('text-anchor', 'middle')
        .attr('x', (_, i) => cur_x[i])
        .attr('y', (_, i) => cur_y[i])
        .style('fill', 'black')
        .style('font-family', 'sans-serif')
        .style('font-size', '9px')
        .style('font-weight', '600')
        .attr('text-anchor', 'middle')
        .text((d, i) => {
            var cur_text = next_data[i]
            if ( cur_text.length > 25 ) {
                return cur_text.slice(0, 25) + '...'
            }
            else
            {
                return cur_text.slice(0, 25)
            }
        })
        .style('cursor', 'pointer')
        .on('click', (d, i) => selectChartIndex(i))
        .on('mouseover', (d, i) => {
            document.getElementById("index"+i).setAttribute('r', 14);
        })
        .on('mouseleave', (d, i) => {
            document.getElementById("index"+i).setAttribute('r', 11);
        })
        .style('cursor', 'pointer')
        .on('click', (d, i) => selectChartIndex(i))
        .on('mouseover', (d, i) => {
            document.getElementById("index"+i).setAttribute('r', 14);
        })
        .on('mouseleave', (d, i) => {
            document.getElementById("index"+i).setAttribute('r', 11);
        })
    
    return label_elem;
};

// Axial lines and labels, giving a base to display the individual metrics
// associated with each maturity axis.

export const axes = (dimensions, metrics, surface) => {
    const _common = common(metrics, surface);
    const _lines = lines(_common, dimensions);
    const _labels = labels(_common, dimensions, metrics);

    // Alexy Improvments - Move labels to current value position
    var i = 0;
    for ( let elem of surface._groups[0][0].children[1].children ) {
        // x
        elem.children[1].attributes[1].value = cartesian(dimensions.arc, i, dimensions.scale(max(dimensions.range) * metrics[i].actual/max(dimensions.range) )).x();
        elem.children[2].attributes[1].value = cartesian(dimensions.arc, i, dimensions.scale(max(dimensions.range) * metrics[i].actual/max(dimensions.range) )).x();
        // y
        elem.children[1].attributes[2].value = cartesian(dimensions.arc, i, dimensions.scale(max(dimensions.range) * metrics[i].actual/max(dimensions.range) )).y() + 35;
        elem.children[2].attributes[2].value = cartesian(dimensions.arc, i, dimensions.scale(max(dimensions.range) * metrics[i].actual/max(dimensions.range) )).y() + 47;

        i = i + 1;
    }

    // Alexy Improvments - Reduce stroke lines
    i = 0;
    //console.log(surface._groups[0][0].children[1].children);
    for ( let elem of surface._groups[0][0].children[1].children ) {
        // x
        elem.children[0].attributes[2].value = cartesian(dimensions.arc, i, dimensions.scale(max(dimensions.range) * metrics[i].actual/max(dimensions.range) )).x();
        // y
        elem.children[0].attributes[3].value = cartesian(dimensions.arc, i, dimensions.scale(max(dimensions.range) * metrics[i].actual/max(dimensions.range) )).y();

        i = i + 1;
    }

    return surface;
};
