import { select } from 'd3-selection';
import { curveLinearClosed, lineRadial } from 'd3-shape';
import { cartesian } from '../position';

const common = (data) => {
    return data
        .append('g')
        .attr('class', 'shape');
};

const line = ({ arc, scale }, values) => {
    return lineRadial(values)
        .curve(curveLinearClosed)
        .radius((d) => scale(d))
        .angle((d, i) => i * arc);
};

const plot = (common, line, { colour, hover, opacity }, values) => {
    const _plot = common
          .append('path')
          .attr('class', 'path')
          .attr('d', () => line(values))
          .style('fill', colour)
          .style('fill-opacity', 0)
          .style('stroke', colour)
          .style('stroke-opacity', 2 * opacity)
          .style('stroke-width', 0);

    return hover ? _plot : _plot;
};

const markers = (common, { arc, scale }, { colour, opacity }, values, metrics) => {
    return common
        .selectAll('.markers')
        .data(values)
        .enter()
        .append('circle')
        .attr('r', 11)
        .attr('cx', (d, i) => cartesian(arc, i, scale(d)).x())
        .attr('cy', (d, i) => cartesian(arc, i, scale(d)).y())
        .style('fill', colour)
        .style('transition-duration', '0.5s')
        .style('fill-opacity', 1)
        .style('cursor', 'pointer')
        .attr('id', (d, i) => "index" + i)
        .style('-webkit-filter', 'drop-shadow( 0px 0px 8px ' + colour + ')')
        .on('click', (d, i) => selectChartIndex(i))
        .on('mouseover', (d, i) => {
            document.getElementById("index"+i).setAttribute('r', 14);
        })
        .on('mouseleave', (d, i) => {
            document.getElementById("index"+i).setAttribute('r', 11);
        })
};

export const shape = (data, dimensions, style, values, metrics) => {
    const _common = common(data);
    const _line = line(dimensions, values);
    //const _plot = plot(_common, _line, style, values);
    const _markers = markers(_common, dimensions, style, values, metrics);

    console.log(data)

    return data;
};
